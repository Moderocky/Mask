package com.moderocky.mask.api.fx;

public interface Application<T extends Frame> extends Resourceful {

    String getName();

    T[] getFrames();

    T getFrame(String id);

    boolean hasFrame(String id);

}
