package com.moderocky.mask.api.fx;

import java.io.*;
import java.nio.charset.StandardCharsets;

public interface Frame extends Resourceful {

    String getID();

}
