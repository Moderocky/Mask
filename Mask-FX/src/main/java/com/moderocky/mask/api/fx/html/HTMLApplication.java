package com.moderocky.mask.api.fx.html;

import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.fx.Application;
import com.moderocky.mask.api.fx.Frame;

public class HTMLApplication implements Application<Page> {

    private final MagicMap<String, Page> pages = new MagicMap<>();

    protected void createPage(Page page) {
        pages.put(page.id, page);
    }

    public Page getPage(String id) {
        return pages.get(id);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public Page[] getFrames() {
        return new Page[0];
    }

    @Override
    public Page getFrame(String id) {
        return null;
    }

    @Override
    public boolean hasFrame(String id) {
        return false;
    }


}
