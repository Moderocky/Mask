package com.moderocky.mask.internal.utility;

import com.moderocky.mask.annotation.Internal;
import com.moderocky.mask.api.Compressive;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * A few frequently-used file methods.
 */
@Internal
public class FileManager implements Compressive {

    public static final FileManager MANAGER = new FileManager();

    public static void putIfAbsent(@NotNull File file) {
        if (!file.exists()) {
            try {
                if (file.getParentFile() != null) file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException ignore) {
            }
        }
    }

    public static void clear(@NotNull File file) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.print("");
        } catch (Exception ignore) {
        }
    }

    public static InputStreamReader getReader(@NotNull File file) {
        try {
            return new InputStreamReader(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        }
    }

    public static void writeCompressed(File file, String content) {
        writeBytes(file, MANAGER.zip(content));
    }

    public static void writeBytes(File file, byte[] content) {
        write(file, Base64.getEncoder().encodeToString(content));
    }

    public static void write(File file, String content) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.print(content);
        } catch (Exception ignore) {
        }
    }

    public static String readCompressed(File file) {
        return MANAGER.unzip(readBytes(file));
    }

    public static byte[] readBytes(File file) {
        return Base64.getDecoder().decode(read(file).getBytes(StandardCharsets.UTF_8));
    }

    public static String read(File file) {
        StringBuilder builder = new StringBuilder();
        try (FileReader reader = new FileReader(file)) {
            while (reader.ready()) {
                char c = (char) reader.read();
                builder.append(c);
            }
        } catch (IOException ignore) {
        }
        return builder.toString();
    }

    @NotNull
    public static String read(InputStream stream) {
        try (Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
             BufferedReader input = new BufferedReader(reader)) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = input.readLine()) != null) {
                builder.append(line);
                builder.append(System.lineSeparator());
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (Throwable ignore) {
            }
        }
        return "";
    }

    @NotNull
    public static String getContent(@NotNull File file) {
        try (FileInputStream stream = new FileInputStream(file);
             Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
             BufferedReader input = new BufferedReader(reader)) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = input.readLine()) != null) {
                builder.append(line);
                builder.append(System.lineSeparator());
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
