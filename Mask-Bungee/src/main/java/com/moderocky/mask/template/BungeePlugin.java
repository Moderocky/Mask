package com.moderocky.mask.template;

import net.md_5.bungee.api.plugin.Plugin;
import org.bstats.bungeecord.Metrics;

import java.io.InputStream;

public abstract class BungeePlugin extends Plugin implements IPlugin {

    private final Metrics maskMetrics = new Metrics(this, 7187);

    @Override
    public void onEnable() {
        startup();
    }

    @Override
    public void onDisable() {
        disable();
    }


    @Override
    public InputStream getResource(String s) {
        return getResourceAsStream(s);
    }

}
