package com.moderocky.mask.command;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Moderocky
 * @version 1.0.0
 */
public class ArgBungeePlayer implements Argument<ProxiedPlayer> {

    private String label = "player";
    private boolean required = true;

    @Override
    public @NotNull ProxiedPlayer serialise(String string) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(string);
        if (player == null) throw new IllegalArgumentException();
        return player;
    }

    @Override
    public boolean matches(String string) {
        return (ProxyServer.getInstance().getPlayer(string) != null);
    }

    @Override
    public @NotNull String getName() {
        return label;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        List<String> players = new ArrayList<>();
        ProxyServer.getInstance().getPlayers().forEach(player -> players.add(player.getName()));
        return players;
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgBungeePlayer setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgBungeePlayer setLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

}