package com.moderocky.mask.command;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Moderocky
 * @version 1.0.0
 */
public class ArgBungeePlugin implements Argument<Plugin> {

    private String label = "server";
    private boolean required = true;

    @Override
    public @NotNull Plugin serialise(String string) {
        Plugin plugin = ProxyServer.getInstance().getPluginManager().getPlugin(string);
        if (plugin == null) throw new IllegalArgumentException();
        return plugin;
    }

    @Override
    public boolean matches(String string) {
        return (ProxyServer.getInstance().getPluginManager().getPlugin(string) != null);
    }

    @Override
    public @NotNull String getName() {
        return label;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        List<String> strings = new ArrayList<>();
        ProxyServer.getInstance().getPluginManager().getPlugins().forEach(plugin -> strings.add(plugin.getDescription().getName()));
        return strings;
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgBungeePlugin setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgBungeePlugin setLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

}