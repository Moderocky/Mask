package com.moderocky.mask.config;

import java.util.function.DoublePredicate;

public interface WritableSection extends Section {

    default <T> T getValue(String key, Class<T> cls) {
        return getValue(key, cls, null);
    }

    default <T> T getValue(String key, Class<T> cls, T def) {
        if (cls == String.class) return cls.cast(getString(key, (String) def));
        if (cls == boolean.class) return cls.cast(getBoolean(key, (Boolean) def));
        if (cls == byte.class) return cls.cast(getByte(key, (Byte) def));
        if (cls == char.class) return cls.cast(getChar(key, (Character) def));
        if (cls == double.class) return cls.cast(getDouble(key, (Double) def));
        if (cls == float.class) return cls.cast(getFloat(key, (Float) def));
        if (cls == int.class) return cls.cast(getInt(key, (Integer) def));
        if (cls == long.class) return cls.cast(getLong(key, (Long) def));
        if (cls == short.class) return cls.cast(getShort(key, (Short) def));
        return cls.cast(this.<T>getValue(key));
    }

    default <T> T getArray(String key, Class<T> cls, T def) {
        if (cls == String.class) return cls.cast(getString(key, (String) def));
        if (cls == boolean.class) return cls.cast(getBoolean(key, (Boolean) def));
        if (cls == byte.class) return cls.cast(getByte(key, (Byte) def));
        if (cls == char.class) return cls.cast(getChar(key, (Character) def));
        if (cls == double.class) return cls.cast(getDouble(key, (Double) def));
        if (cls == float.class) return cls.cast(getFloat(key, (Float) def));
        if (cls == int.class) return cls.cast(getInt(key, (Integer) def));
        if (cls == long.class) return cls.cast(getLong(key, (Long) def));
        if (cls == short.class) return cls.cast(getShort(key, (Short) def));
        return cls.cast(this.<T>getValue(key));
    }

    <T> T getValue(String key);

    default <T> void setValue(String key, T value) {
        if (value instanceof String) set(key, (String) value);
        if (value instanceof Boolean) set(key, (Boolean) value);
        if (value instanceof Byte) set(key, (Byte) value);
        if (value instanceof Character) set(key, (Character) value);
        if (value instanceof Double) set(key, (Double) value);
        if (value instanceof Float) set(key, (Float) value);
        if (value instanceof Integer) set(key, (Integer) value);
        if (value instanceof Long) set(key, (Long) value);
        if (value instanceof Short) set(key, (Short) value);
    }

    void set(String key, String value);
    void set(String key, Boolean value);
    void set(String key, Byte value);
    void set(String key, Character value);
    void set(String key, Double value);
    void set(String key, Float value);
    void set(String key, Integer value);
    void set(String key, Long value);
    void set(String key, Short value);

    String getString(String key, String def);
    boolean getBoolean(String key, Boolean def);
    byte getByte(String key, Byte def);
    char getChar(String key, Character def);
    double getDouble(String key, Double def);
    float getFloat(String key, Float def);
    int getInt(String key, Integer def);
    long getLong(String key, Long def);
    short getShort(String key, Short def);

    void set(String key, String... value);
    void set(String key, boolean... value);
    void set(String key, byte... value);
    void set(String key, char... value);
    void set(String key, double... value);
    void set(String key, float... value);
    void set(String key, int... value);
    void set(String key, long... value);
    void set(String key, short... value);

}
