package com.moderocky.mask.api.chat;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public interface ITag {

    boolean matches(String string);

    BaseComponent convert(String string);

    default void work(final ComponentBuilder builder, String string) {
        builder.append(convert(string)); // Generally we just convert
    } // SOME tags (like <reset> and <bold> etc.) may have special implementations!

}
