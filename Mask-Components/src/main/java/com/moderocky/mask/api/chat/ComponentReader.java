package com.moderocky.mask.api.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComponentReader {

    public static final List<ITag> TAGS;
    private static final Pattern colour = Pattern.compile("(<#[a-fA-F0-9]{6}>)"); // Regex for hex colours

    static {
        TAGS = new ArrayList<>();
        TAGS.addAll(Arrays.asList(Tag.values()));
        for (ChatColor value : ChatColor.values()) { // LEGACY Chat colours by name. i.e. <red>, <italic>, etc.
            if (value == ChatColor.RESET) continue; // We have our own reset system.
            ComponentReader.register(new ITag() {
                @Override
                public boolean matches(String string) { // assembles <red> / <italic> / etc.
                    return string.trim().equalsIgnoreCase("<" + value.getName() + ">");
                }

                @Override
                public BaseComponent convert(String string) { // This doesn't actually work, it resets the formatting.
                    return new TextComponent(value + "");
                }

                @Override
                public void work(ComponentBuilder builder, String string) {
                    builder.append(TextComponent.fromLegacyText(value.toString()));
                } // Doing this maintains the formatting properly
            });
        }
    }

    private final String string; // The input text, so it can be re-compiled if needed
    private BaseComponent[] components = new BaseComponent[0];

    public ComponentReader(final String string) {
        this.string = string;
        convert();
    }

    public static void register(ITag tag) {
        if (!TAGS.contains(tag)) TAGS.add(tag); // Don't want to register it if it already exists!
    }                                           // Sadly, anonymous tags would be re-registered.

    public void convert() {
        String working = string;
        final ComponentBuilder builder = new ComponentBuilder().color(ChatColor.WHITE); // This makes formatting work properly, for some reason...
        Matcher matcher = colour.matcher(working);
        while (matcher.find()) { // We do colours FIRST because they need to be added in as strings.
            String group = matcher.group();
            working = working.replace(group, ChatColor.of(group.substring(1, group.length() - 1)).toString());
        }
        StringReader reader = new StringReader(working); // This reads the string char-by-char
        StringBuilder buffer = new StringBuilder();
        StringReader checker;
        while (reader.canRead()) { // Now we check the tags
            events:
            {
                char c = reader.rotate(); // Gives us the char at the pointer, moves the pointer along.
                if (c == '<') { // Probably the start of the tag.
                    checker = reader.clone(); // We clone the reader
                    String string = "<" + (checker.readUntil('>').trim() + ">"); // Keep going until a '>' is reached.
                    checker.skip(); // The pointer is on '>' currently, we need to kick it along by 1
                    for (ITag value : TAGS) { // Loop all tags (built-in and registered ones)
                        if (value.matches(string)) { // we've found the tag
                            if (!buffer.toString().isEmpty()) // Have we been parsing text already? if so we need to add it first
                                builder.append(TextComponent.fromLegacyText(buffer.toString()));
                            value.work(builder, string); // Different tags might have different implementations, to be safe, we give them the component builder
                            buffer = new StringBuilder(); // Reset the buffer, we've transferred it into the component builder already
                            reader.skip(string.length() - 1); // skip the <tag-length>, so our pointer is now after the '>'
                            break events; // we don't want to do any more checks.
                        }
                    }
                }
                buffer.append(c); // Not a '<'? we just add this to the buffer.
            }
        } // If the buffer isn't empty, add it to the component builder.
        if (!buffer.toString().isEmpty()) builder.append(TextComponent.fromLegacyText(buffer.toString()));
        components = builder.create(); // finalise it
    }

    public BaseComponent[] read() {
        return components; // We store the finished component as a field so we can re-use the same thing.
    }

    public BaseComponent[] withoutEvents() { // Strips click/hover events.
        BaseComponent[] components = this.components.clone();
        for (BaseComponent component : components) {
            component.setClickEvent(null);
            component.setHoverEvent(null);
        }
        return components;
    }

    public String toLegacy() { // Helpful method.
        return TextComponent.toLegacyText(components);
    }

    public String toPlainText() { // Helpful method.
        return TextComponent.toPlainText(components);
    }

}
