package com.moderocky.mask.api;

import com.moderocky.mask.annotation.Debug;
import dev.moderocky.mirror.ConstructorMirror;
import dev.moderocky.mirror.FieldMirror;
import dev.moderocky.mirror.MethodMirror;
import dev.moderocky.mirror.Mirror;

import java.util.Arrays;

@SuppressWarnings("all")
public class Debugger {

    public <T> String getDebug(Class<T> cls) {
        return getDebug(cls, false);
    }

    public <T> String getDebug(Class<T> cls, boolean force) {
        final Mirror<Class<T>> mirror = new Mirror<>(cls);
        final MagicStringList strings = new MagicStringList();
        final boolean debugAll = force || (mirror.hasAnnotation(Debug.class) && mirror.getAnnotation(Debug.class).value());
        strings.add("Class: " + cls.getCanonicalName() + " (" + cls.getPackage() + ")");
        strings.add("  Fields:");
        for (FieldMirror<Object> field : mirror.getFieldMirrors()) {
            if (!field.isStatic()) continue;
            if (!debugAll && !field.hasAnnotation(Debug.class) || (field.hasAnnotation(Debug.class) && !field.getAnnotation(Debug.class).value()))
                continue;
            StringBuilder modifiers = new StringBuilder();
            if (field.isPublic()) modifiers.append("public ");
            else if (field.isProtected()) modifiers.append("protected ");
            else if (field.isPrivate()) modifiers.append("private ");
            else if (field.isStatic()) modifiers.append("static ");
            else if (field.isNative()) modifiers.append("native ");
            else if (field.isTransient()) modifiers.append("transient ");
            else if (field.isFinal()) modifiers.append("final ");
            else if (field.isVolatile()) modifiers.append("volatile ");
            strings.add("    " + modifiers + field.getType().getName() + " " + field.getName() + " = " + field.tryFunc(reflective -> field.get().toString(), "Unknown"));
        }
        strings.add("  Constructors:");
        for (ConstructorMirror<Object> constructor : mirror.getConstructorMirrors()) {
            if (!debugAll && !constructor.hasAnnotation(Debug.class) || (constructor.hasAnnotation(Debug.class) && !constructor.getAnnotation(Debug.class).value()))
                continue;
            StringBuilder modifiers = new StringBuilder();
            if (constructor.isPublic()) modifiers.append("public ");
            else if (constructor.isProtected()) modifiers.append("protected ");
            else if (constructor.isPrivate()) modifiers.append("private ");
            strings.add("    " + modifiers + constructor.getName() + "#" + Arrays.toString(constructor.getParameters()));
        }
        strings.add("  Static Methods:");
        for (MethodMirror<Object> method : mirror.getMethodMirrors()) {
            if (!method.isStatic()) continue;
            if (!debugAll && !method.hasAnnotation(Debug.class) || (method.hasAnnotation(Debug.class) && !method.getAnnotation(Debug.class).value()))
                continue;
            StringBuilder modifiers = new StringBuilder();
            if (method.isPublic()) modifiers.append("public ");
            else if (method.isProtected()) modifiers.append("protected ");
            else if (method.isPrivate()) modifiers.append("private ");
            else if (method.isStatic()) modifiers.append("static ");
            else if (method.isNative()) modifiers.append("native ");
            else if (method.isTransient()) modifiers.append("transient ");
            else if (method.isFinal()) modifiers.append("final ");
            else if (method.isVolatile()) modifiers.append("volatile ");
            strings.add("    " + modifiers + method.getReturnType().getName() + " " + method.getName() + "#" + Arrays.toString(method.getParameters()));
        }
        return String.join("\n", strings);
    }

    public String getDebug(Object object) {
        return getDebug(object, false);
    }

    public <T> String getDebug(T object, boolean force) {
        final Mirror<T> mirror = new Mirror<>(object);
        final Class<T> clarse = (Class<T>) object.getClass();
        final MagicStringList strings = new MagicStringList();
        final boolean debugAll = force || (mirror.hasAnnotation(Debug.class) && mirror.getAnnotation(Debug.class).value());
        strings.add("Object: " + clarse.getCanonicalName() + " (" + clarse.getPackage() + ")");
        strings.add("  Instance Fields:");
        for (FieldMirror<Object> field : mirror.getFieldMirrors()) {
            if (field.isStatic()) continue;
            if (!debugAll && !field.hasAnnotation(Debug.class) || (field.hasAnnotation(Debug.class) && !field.getAnnotation(Debug.class).value()))
                continue;
            StringBuilder modifiers = new StringBuilder();
            if (field.isPublic()) modifiers.append("public ");
            else if (field.isProtected()) modifiers.append("protected ");
            else if (field.isPrivate()) modifiers.append("private ");
            else if (field.isStatic()) modifiers.append("static ");
            else if (field.isNative()) modifiers.append("native ");
            else if (field.isTransient()) modifiers.append("transient ");
            else if (field.isFinal()) modifiers.append("final ");
            else if (field.isVolatile()) modifiers.append("volatile ");
            strings.add("    " + modifiers + field.getType().getName() + " " + field.getName() + " = " + field.tryFunc(reflective -> field.get().toString(), "Unknown"));
        }
        strings.add("  Instance Methods:");
        for (MethodMirror<Object> method : mirror.getMethodMirrors()) {
            if (method.isStatic()) continue;
            if (!debugAll && !method.hasAnnotation(Debug.class) || (method.hasAnnotation(Debug.class) && !method.getAnnotation(Debug.class).value()))
                continue;
            StringBuilder modifiers = new StringBuilder();
            if (method.isPublic()) modifiers.append("public ");
            else if (method.isProtected()) modifiers.append("protected ");
            else if (method.isPrivate()) modifiers.append("private ");
            else if (method.isStatic()) modifiers.append("static ");
            else if (method.isNative()) modifiers.append("native ");
            else if (method.isTransient()) modifiers.append("transient ");
            else if (method.isFinal()) modifiers.append("final ");
            else if (method.isVolatile()) modifiers.append("volatile ");
            strings.add("    " + modifiers + method.getReturnType().getName() + " " + method.getName() + "#" + Arrays.toString(method.getParameters()));
        }
        return String.join("\n", strings);
    }

}
