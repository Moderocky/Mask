package com.moderocky.mask.gui;

import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;

public interface InventoryGUI extends GUI {

    int getSize();

    boolean isEditable();

    InventoryType getType();

}
