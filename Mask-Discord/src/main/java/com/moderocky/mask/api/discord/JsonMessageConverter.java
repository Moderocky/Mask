package com.moderocky.mask.api.discord;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.time.Instant;

public class JsonMessageConverter {

    public static final JsonMessageConverter CONVERTER = new JsonMessageConverter();

    private JsonMessageConverter() {
    }

    public MessageBuilder toMessageBuilder(JsonObject object) {
        MessageBuilder builder = new MessageBuilder();
        if (!isNull(object, "content"))
            builder.setContent(object.get("content").getAsString());
        if (object.has("embed") && object.get("embed").isJsonObject())
            builder.setEmbed(toEmbed(object.getAsJsonObject("embed")));
        return builder;
    }

    public Message toMessage(JsonObject object) {
        return toMessageBuilder(object).build();
    }

    public JsonObject toJson(Message message) {
        JsonObject object = new JsonObject();
        if (message.getContentRaw().length() > 0)
            object.addProperty("content", message.getContentRaw());
        if (message.getEmbeds().size() > 0)
            object.add("embed", toJson(message.getEmbeds().iterator().next()));
        return object;
    }

    public EmbedBuilder toEmbedBuilder(JsonObject object) {
        EmbedBuilder builder = new EmbedBuilder();
        if (object.has("author") && object.get("author").isJsonObject()) {
            try {
                JsonObject author = object.getAsJsonObject("author");
                String name = author.has("name") ? author.get("name").getAsString() : null;
                String url = author.has("url") ? author.get("url").getAsString() : null;
                String icon = author.has("icon_url") ? author.get("icon_url").getAsString() : null;
                builder.setAuthor(name, url, icon);
            } catch (Throwable ignore) {
            }
        }
        String url = !isNull(object, "url") ? object.get("url").getAsString() : null;
        builder.setTitle(!isNull(object, "title") ? object.get("title").getAsString() : null, url);
        builder.setDescription(!isNull(object, "description") ? object.get("description").getAsString() : null);
        builder.setColor(!isNull(object, "colour") ? object.get("colour").getAsInt() : 0);
        builder.setTimestamp(!isNull(object, "timestamp") ? Instant.parse(object.get("timestamp").getAsString()) : null);
        if (object.has("thumbnail") && object.get("thumbnail").isJsonObject()) {
            builder.setThumbnail(object.getAsJsonObject("thumbnail").get("url").getAsString());
        } else {
            builder.setThumbnail(!isNull(object, "thumbnail") ? object.get("thumbnail").getAsString() : null);
        }
        if (object.has("image") && object.get("image").isJsonObject()) {
            builder.setImage(object.getAsJsonObject("image").get("url").getAsString());
        } else {
            builder.setImage(!isNull(object, "image") ? object.get("image").getAsString() : null);
        }
        if (object.has("footer") && object.get("footer").isJsonObject()) {
            try {
                JsonObject footer = object.getAsJsonObject("footer");
                String text = footer.has("text") ? footer.get("text").getAsString() : null;
                String icon = footer.has("icon_url") ? footer.get("icon_url").getAsString() : null;
                builder.setFooter(text, icon);
            } catch (Throwable ignore) {
            }
        }
        if (object.has("fields") && object.get("fields").isJsonArray()) {
            JsonArray fields = object.getAsJsonArray("fields");
            for (JsonElement element : fields) {
                try {
                    JsonObject field = element.getAsJsonObject();
                    String name = field.has("name") ? field.get("name").getAsString() : null;
                    String value = field.has("value") ? field.get("url").getAsString() : null;
                    boolean inline = field.has("inline") && field.get("inline").getAsBoolean();
                    builder.addField(name, value, inline);
                } catch (Throwable ignore) {
                }
            }
        }
        return builder;
    }

    public MessageEmbed toEmbed(JsonObject object) {
        return toEmbedBuilder(object).build();
    }

    public JsonObject toJson(MessageEmbed embed) {
        JsonObject object = new JsonObject();
        if (embed.getAuthor() != null) {
            JsonObject author = new JsonObject();
            author.addProperty("name", embed.getAuthor().getName());
            author.addProperty("url", embed.getAuthor().getUrl());
            author.addProperty("icon_url", embed.getAuthor().getIconUrl());
            object.add("author", author);
        }
        object.addProperty("title", embed.getTitle());
        object.addProperty("url", embed.getUrl());
        object.addProperty("description", embed.getDescription());
        object.addProperty("colour", embed.getColorRaw());
        if (embed.getThumbnail() != null) {
            JsonObject thumbnail = new JsonObject();
            thumbnail.addProperty("url", embed.getThumbnail().getUrl());
            object.add("thumbnail", thumbnail);
        }
        if (embed.getImage() != null) {
            JsonObject image = new JsonObject();
            image.addProperty("url", embed.getImage().getUrl());
            object.add("image", image);
        }
        object.addProperty("timestamp", embed.getTimestamp() != null ? embed.getTimestamp().toString() : null);
        if (embed.getFooter() != null) {
            JsonObject footer = new JsonObject();
            footer.addProperty("text", embed.getFooter().getText());
            footer.addProperty("icon_url", embed.getFooter().getIconUrl());
            object.add("footer", footer);
        }
        if (embed.getFields().size() > 0) {
            JsonArray fields = new JsonArray();
            for (MessageEmbed.Field embedField : embed.getFields()) {
                JsonObject field = new JsonObject();
                field.addProperty("name", embedField.getName());
                field.addProperty("value", embedField.getValue());
                field.addProperty("inline", embedField.isInline());
                fields.add(field);
            }
            object.add("fields", fields);
        }
        return object;
    }

    private boolean isNull(JsonObject object, String key) {
        return (!object.has(key) || object.get(key).isJsonNull());
    }

}
