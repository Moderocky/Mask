package com.moderocky.mask.api.discord;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.requests.RestAction;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.CompletableFuture;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class DummyRestAction<T> implements RestAction<T> {

    private final JDA jda;

    private T completion = null;
    private Consumer<? super T> success = t -> {
    };
    private Consumer<? super Throwable> failure = throwable -> {
    };

    public DummyRestAction(Class<T> cls, JDA jda) {
        this.jda = jda;
    }

    public DummyRestAction(T t, JDA jda) {
        this.jda = jda;
        this.completion = t;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        return jda;
    }

    @Nonnull
    @Override
    public RestAction<T> setCheck(@Nullable BooleanSupplier checks) {
        return this;
    }

    @Override
    public void queue(@Nullable Consumer<? super T> success, @Nullable Consumer<? super Throwable> failure) {
        if (success != null) this.success = success;
        if (failure != null) this.failure = failure;
    }

    @Override
    public T complete(boolean shouldQueue) throws RateLimitedException {
        return completion;
    }

    public void internalComplete(T t) {
        this.completion = t;
        try {
            success.accept(t);
        } catch (Throwable throwable) {
            failure.accept(throwable);
        }
    }

    @Nonnull
    @Override
    public CompletableFuture<T> submit(boolean shouldQueue) {
        return new CompletableFuture<>();
    }
}
