package com.moderocky.mask.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class MagicMap<K, V> extends LinkedHashMap<K, V> {

    public MagicMap() {
        super();
    }

    public MagicMap(Map<K, V> map) {
        super(map);
    }

    public MagicMap(List<K> keys, List<V> values) {
        for (int i = 0; i < Math.min(keys.size(), values.size()); i++) {
            put(keys.get(i), values.get(i));
        }
    }

    public MagicMap(Collection<K> keys, Collection<V> values) {
        Object[] ks = keys.toArray();
        Object[] vs = values.toArray();
        for (int i = 0; i < Math.min(ks.length, vs.length); i++) {
            put((K) ks[i], (V) vs[i]);
        }
    }

    public static <V> MagicMap<Integer, V> ofIndices(Collection<V> collection) {
        MagicMap<Integer, V> map = new MagicMap<>();
        int i = 0;
        for (V v : collection) {
            map.put(i, v);
            i++;
        }
        return map;
    }

    public boolean removeIf(Predicate<? super K> filter) {
        int a = size();
        for (K key : new ArrayList<>(keySet())) {
            if (filter.test(key)) remove(key);
        }
        return size() != a;
    }

    public boolean removeEntryIf(Predicate<? super Map.Entry<? super K, ? super V>> filter) {
        return entrySet().removeIf(filter);
    }

    public Map.Entry<K, V> remove(int position) {
        if (position >= size()) throw new IllegalArgumentException("Index is out of bounds!");
        int i = 0;
        for (K k : new HashSet<>(keySet())) {
            if (i == position) {
                final V v = get(k);
                remove(k);
                return new Map.Entry<K, V>() {
                    @Override
                    public K getKey() {
                        return k;
                    }

                    @Override
                    public V getValue() {
                        return v;
                    }

                    @Override
                    public V setValue(V value) {
                        return null;
                    }
                };
            }
            i++;
        }
        throw new IllegalArgumentException("Index is out of bounds!");
    }

    public Map.Entry<K, V> getFirst() {
        return get(0);
    }

    public Map.Entry<K, V> getLast() {
        return get(size() - 1);
    }

    public Map.Entry<K, V> get(int position) {
        if (position >= size() || position < 0) throw new IllegalArgumentException("Index is out of bounds!");
        int i = 0;
        for (K k : keySet()) {
            if (i == position) {
                return new Map.Entry<K, V>() {
                    @Override
                    public K getKey() {
                        return k;
                    }

                    @Override
                    public V getValue() {
                        return get(k);
                    }

                    @Override
                    public V setValue(V value) {
                        return replace(k, value);
                    }
                };
            }
            i++;
        }
        throw new IllegalArgumentException("Index is out of bounds!");
    }

    public JsonObject toJsonObject() {
        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        for (Map.Entry<K, V> entry : this.entrySet()) {
            if (entry.getValue() instanceof String)
                object.add(entry.getKey().toString(), new JsonPrimitive(entry.getValue().toString()));
            else if (entry.getValue() instanceof Integer)
                object.add(entry.getKey().toString(), new JsonPrimitive((Integer) entry.getValue()));
            else if (entry.getValue() instanceof Number)
                object.add(entry.getKey().toString(), new JsonPrimitive((Number) entry.getValue()));
            else if (entry.getValue() instanceof Boolean)
                object.add(entry.getKey().toString(), new JsonPrimitive((Boolean) entry.getValue()));
            else
                object.add(entry.getKey().toString(), gson.toJsonTree(entry.getValue()));
        }
        return object;
    }

    public JsonObject toJsonObject(Function<V, JsonElement> converter) {
        JsonObject object = new JsonObject();
        for (Map.Entry<K, V> entry : this.entrySet()) {
            object.add(entry.getKey().toString(), converter.apply(entry.getValue()));
        }
        return object;
    }

}
