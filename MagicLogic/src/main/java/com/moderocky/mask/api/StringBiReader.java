package com.moderocky.mask.api;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

public class StringBiReader {
    public final char[] chars;
    final Marker forward;
    final Marker reverse;

    StringBiReader(MagicStringList list) {
        this(list.toString().toCharArray());
    }

    public StringBiReader(String string) {
        this(string.toCharArray());
    }

    public StringBiReader(final char[] chars) {
        this.chars = chars;
        forward = new Marker((byte) 0);
        reverse = new Marker((byte) 1);
    }

    public void reset() {
        forward.position = 0;
        reverse.position = 0;
    }

    public int charCount(char c) {
        int i = 0;
        for (char ch : chars) {
            if (ch == c) i++;
        }
        return i;
    }

    @Override
    public String toString() {
        return new String(chars);
    }

    @Override
    @SuppressWarnings("all")
    public StringBiReader clone() {
        StringBiReader reader = new StringBiReader(chars);
        reader.forward.position = forward.position;
        reader.reverse.position = reverse.position;
        return reader;
    }

    public StringReader getReader(byte direction) {
        if (direction == 1) return reverse;
        return forward;
    }

    private static char[] reverse(char[] chars) {
        final char[] chs = new char[chars.length];
        int j = chars.length-1;
        for (char aChar : chars) {
            chs[j] = aChar;
            j--;
        }
        return chs;
    }

    protected class Marker extends StringReader {
        transient byte direction;

        private Marker(byte dir) {
            super(dir == 0 ? StringBiReader.this.chars : reverse(StringBiReader.this.chars));
            direction = dir;
            position = 0;
        }
    }

}
