package com.moderocky.mask.api.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dev.moderocky.mirror.Mirror;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.Array;

public class FixedTable<P> implements ITable<P> {

    protected final int rows;
    protected final int columns;
    protected final Class<P> cls;
    protected final @NotNull P @NotNull [][] table;
    protected String name;

    @SuppressWarnings("unchecked")
    public FixedTable(Class<P> cls, int width, int height) {
        this.rows = height;
        this.columns = width;
        this.cls = cls;
        table = (P[][]) Array.newInstance(cls, width, height);
    }

    @SuppressWarnings("unchecked")
    public FixedTable(@NotNull P @NotNull [][] structure) {
        assert structure.length > 0;
        this.rows = structure.length;
        this.columns = structure[0].length;
        this.cls = (Class<P>) structure.getClass().getComponentType().getComponentType();
        this.table = structure;
    }

    public static <T> FixedTable<T> from(File file) {
        return from(file, null);
    }

    public static <T> FixedTable<T> from(String content) {
        return from(content, null);
    }

    public static <T> FixedTable<T> from(File file, @Nullable FixedTable<T> def) {
        String content = ITable.readContent(file);
        return from(content, def);
    }

    public static void to(File file, FixedTable<?> table) {
        ITable.writeContent(file, table.serialise());
    }

    @SuppressWarnings("unchecked")
    public static <T> FixedTable<T> from(String content, @Nullable FixedTable<T> def) {
        final int cols;
        final int rows;
        if (content == null) return def;
        try {
            JsonObject object = JsonParser.parseString(content).getAsJsonObject();
            Class<T> cls = Mirror.getClass(object.get("class").getAsString());
            JsonArray columns = object.getAsJsonArray("table");
            JsonArray size = object.getAsJsonArray("size");
            cols = size.get(0).getAsInt();
            rows = size.get(1).getAsInt();
            final T[][] table = (T[][]) Array.newInstance(cls, cols, rows);
            for (int x = 0; x < columns.size(); x++) {
                JsonArray column = columns.get(x).getAsJsonArray();
                for (int y = 0; y < column.size(); y++) {
                    table[x][y] = GSON.fromJson(column.get(y), cls);
                }
            }
            final T defaultValue = GSON.fromJson(object.get("default"), cls);
            return new FixedTable<T>(table) {
                {
                    name = object.get("name").isJsonNull() ? null : object.get("name").getAsString();
                }

                @Nullable
                @Override
                public T defaultValue() {
                    return defaultValue;
                }
            };
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return def;
        }
    }

    public JsonObject toJson() {
        final JsonObject object = new JsonObject();
        JsonArray size = new JsonArray();
        size.add(rows);
        size.add(columns);
        object.addProperty("name", name);
        object.addProperty("class", cls.getName());
        object.add("size", size);
        object.add("default", GSON.toJsonTree(defaultValue(), cls));
        JsonArray columns = new JsonArray();
        for (P[] col : table) {
            JsonArray column = new JsonArray();
            for (P value : col) {
                column.add(GSON.toJsonTree(value, cls));
            }
            columns.add(column);
        }
        object.add("table", columns);
        return object;
    }

    public String serialise() {
        return toJson().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("all")
    public P get(int x, int y) throws IndexOutOfBoundsException {
        @Nullable P p = table[x][y];
        return p == null ? defaultValue() : p;
    }

    @SuppressWarnings("unchecked")
    public <Q> Q getCast(int x, int y) throws IndexOutOfBoundsException {
        return (Q) get(x, y);
    }

    public void set(int x, int y, final P value) throws IndexOutOfBoundsException {
        table[x][y] = value;
    }

    public Class<P> getComponentType() {
        return cls;
    }

    @SuppressWarnings("unchecked")
    public P[] getRow(int y) {
        final P[] row = (P[]) Array.newInstance(cls, columns);
        for (int i = 0; i < rows; i++) {
            row[i] = table[i][y];
        }
        return row;
    }

    public P[] getColumn(int x) {
        return copy(table[x]);
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public P[][] getTable() {
        return copy(table);
    }

    @SuppressWarnings("unchecked")
    private <Q> Q[] copy(Q[] original) {
        final Q[] copy = (Q[]) Array.newInstance(original.getClass().getComponentType(), original.length);
        System.arraycopy(original, 0, copy, 0, original.length);
        return copy;
    }

    public @Nullable P defaultValue() {
        return null;
    }

    @Override
    public P[] asLinearRepresentation() {
        final P[] array = ITable.newArray(cls, columns*rows);
        int index = 0;
        for (P[] column : table) {
            for (P element : column) {
                array[index] = element;
                index++;
            }
        }
        return array;
    }

}
