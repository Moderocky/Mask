package com.moderocky.mask.api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A specialised ArrayList wrapper with extra features for quick and easy use.
 *
 * @param <T> Collection type
 * @author Mackenzie
 */
public class MagicList<T> extends ArrayList<T> {

    public MagicList() {
        super();
    }

    public MagicList(Collection<? extends T> collection) {
        super(collection);
    }

    @SafeVarargs
    public MagicList(T... ts) {
        super(Arrays.asList(ts));
    }

    public static MagicStringList ofWords(String string) {
        return new MagicStringList(string.split("\\s+"));
    }

    public static MagicStringList from(JsonArray array) {
        MagicStringList list = new MagicStringList();
        for (JsonElement element : array) {
            list.add(element.getAsString());
        }
        return list;
    }

    public static <T> MagicList<T> from(JsonArray array, Function<JsonElement, T> converter) {
        MagicList<T> list = new MagicList<>();
        for (JsonElement element : array) {
            list.add(converter.apply(element));
        }
        return list;
    }

    public static <T> MagicList<T> from(JsonArray array, Class<T> cls) {
        Gson gson = new Gson();
        MagicList<T> list = new MagicList<>();
        for (JsonElement element : array) {
            list.add(gson.fromJson(element, cls));
        }
        return list;
    }

    @SafeVarargs
    public static <T> MagicList<T> from(Collection<T>... collections) {
        MagicList<T> list = new MagicList<>();
        for (Collection<T> collection : collections) {
            list.addAll(collection);
        }
        return list;
    }

    public MagicList<T> from(int start) {
        return from(start, size());
    }

    public MagicList<T> from(int start, int end) {
        if (start < 0) throw new IllegalArgumentException("Start index must be positive!");
        if (end > size()) throw new IllegalArgumentException("End index must not be greater than the list's size!");
        MagicList<T> list = new MagicList<>();
        for (int i = start; i < end; i++) {
            list.add(get(i));
        }
        return list;
    }

    public boolean removeUnless(Predicate<? super T> filter) {
        return this.removeIf(filter.negate());
    }

    public MagicList<T> without(Predicate<? super T> filter) {
        MagicList<T> list = new MagicList<>(this);
        list.removeIf(filter);
        return list;
    }

    public <R> MagicList<R> collectIgnoreNull(Function<T, R> function) {
        MagicList<R> list = new MagicList<>();
        for (T thing : this) {
            R blob = function.apply(thing);
            if (blob != null) list.add(blob);
        }
        return list;
    }

    public boolean oneMatches(Function<T, Boolean> function) {
        for (T t : this) {
            if (function.apply(t)) return true;
        }
        return false;
    }

    public boolean noMatches(Function<T, Boolean> function) {
        for (T t : this) {
            if (!function.apply(t)) return false;
        }
        return true;
    }

    public MagicMap<Integer, T> toIndexMap() {
        return MagicMap.ofIndices(this);
    }

//    public void forEach(ReflectiveOperation<T> function) {
//        forEach(function, null);
//    }
//
//    public void forEach(ReflectiveOperation<T> function, @Nullable Consumer<Throwable> failure) {
//        for (T thing : this) {
//            try {
//                function.attempt(thing);
//            } catch (Throwable throwable) {
//                if (failure != null) failure.accept(throwable);
//            }
//        }
//    }

//    public <R> MagicList<R> collectUnsafe(ReflectiveBiOperation<T, R> function) {
//        return collectUnsafe(function, true);
//    }
//
//    public <R> MagicList<R> collectUnsafe(ReflectiveBiOperation<T, R> function, boolean ignoreFailures) {
//        MagicList<R> list = new MagicList<>();
//        for (T thing : this) {
//            try {
//                list.add(function.attempt(thing));
//            } catch (Throwable throwable) {
//                if (!ignoreFailures) list.add(function.otherwise(thing, throwable));
//            }
//        }
//        return list;
//    }

    public <R> MagicList<R> collect(Function<T, R> function) {
        MagicList<R> list = new MagicList<>();
        for (T thing : this) {
            list.add(function.apply(thing));
        }
        return list;
    }

    public <U, R> MagicList<R> collect(BiFunction<T, U, R> function, U argument) {
        MagicList<R> list = new MagicList<>();
        for (T thing : this) {
            list.add(function.apply(thing, argument));
        }
        return list;
    }

    public <Q> MagicList<Q> castConvert(Class<Q> cls) {
        return castConvert();
    }

    @SuppressWarnings("unchecked")
    public <Q> MagicList<Q> castConvert() {
        MagicList<Q> list = new MagicList<>();
        for (T thing : this) {
            list.add((Q) thing);
        }
        return list;
    }

    public T getRandom() {
        return get(ThreadLocalRandom.current().nextInt(size()));
    }

    public <Q extends Number> MagicNumberList<Q> asMagicNumbers() {
        return new MagicNumberList<>((MagicList<Q>) this);
    }

    public MagicStringList asMagicStrings() {
        MagicStringList list = new MagicStringList();
        if (!this.isEmpty()) {
            list.addAll(this.collect(Objects::toString));
        }
        return list;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T[] toArray() {
        return (T[]) super.toArray();
    }

    @SafeVarargs
    public final void addAll(T... ts) {
        addAll(Arrays.asList(ts));
    }

    @SafeVarargs
    public final void addAll(int i, T... ts) {
        addAll(i, Arrays.asList(ts));
    }

    public JsonArray toJsonStringArray() {
        JsonArray array = new JsonArray();
        for (T thing : this) {
            array.add(thing.toString());
        }
        return array;
    }

    public JsonArray toJsonArray(Function<T, JsonElement> converter) {
        JsonArray array = new JsonArray();
        for (T thing : this) {
            array.add(converter.apply(thing));
        }
        return array;
    }

    public JsonArray toJsonArray() {
        Gson gson = new Gson();
        JsonArray array = new JsonArray();
        for (T thing : this) {
            if (thing instanceof JsonElement)
                array.add((JsonElement) thing);
            else if (thing instanceof String)
                array.add(thing.toString());
            else if (thing instanceof Integer)
                array.add((Integer) thing);
            else if (thing instanceof Number)
                array.add((Number) thing);
            else if (thing instanceof Boolean)
                array.add((Boolean) thing);
            else
                array.add(gson.toJsonTree(thing));
        }
        return array;
    }

    public boolean removeDuplicates() {
        final int s = this.size();
        Set<T> set = new HashSet<>(this);
        this.clear();
        this.addAll(set);
        return s != this.size();
    }

    public MagicList<T> withoutDuplicates() {
        return new MagicList<>(new HashSet<>(this));
    }

    public MagicList<T> getFirst(int amount) {
        return from(0, amount);
    }

    public MagicList<T> getLast(int amount) {
        return from(size()-amount);
    }

    public T getFirst() {
        return get(0);
    }

    public T getLast() {
        return get(this.size() - 1);
    }

    public ArrayList<T> toArrayList() {
        return new ArrayList<>(this);
    }

    public Set<T> toSet() {
        return new HashSet<>(this);
    }

}
