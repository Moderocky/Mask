package com.moderocky.mask.api;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public interface Compressive {

    @SuppressWarnings("deprecation") // Bukkit uses legacy gson!
    JsonParser JSON_PARSER = new JsonParser();

    default byte[] createEmptyJson() {
        return zip(new JsonObject().toString());
    }

    default byte[] zip(String string) {
        if (string == null || string.isEmpty()) return new byte[0];
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(string.length());
             GZIPOutputStream gzip = new GZIPOutputStream(bos)) {
            gzip.write(string.getBytes());
            return bos.toByteArray();
        } catch (IOException iOException) {
            return null;
        }
    }

    default String unzip(byte[] compressed) {
        if (!isZipped(compressed)) return new String(compressed, StandardCharsets.UTF_8);
        try (ByteArrayInputStream stream = new ByteArrayInputStream(compressed);
             GZIPInputStream gzipInputStream = new GZIPInputStream(stream);
             BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8))) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        } catch (IOException iOException) {
            return null;
        }
    }

    default boolean isZipped(byte[] compressed) {
        if (compressed.length == 0) return false;
        return (compressed[0] == 31 && compressed[1] == -117);
    }

}
