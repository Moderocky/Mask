package com.moderocky.example;

import com.moderocky.mask.api.data.FixedNDimensionalTable;
import com.moderocky.mask.api.data.FixedTable;

import java.util.Arrays;
import java.util.regex.Pattern;

import static com.moderocky.example.ExampleLevellingTable.*;

public class Testing {


    public static void main(String[] args) {
        System.out.println("-- CREATION TESTS --");
        {
            {
                new FixedTable<>(new Pattern[5][5]);
                long a = System.nanoTime();
                new FixedTable<>(new Pattern[5][5]);
                long b = System.nanoTime();
                System.out.println("Creation via linked array took: " + (b - a) + " nanoseconds");
            }
            {
                new FixedTable<>(Pattern.class, 5, 5);
                long a = System.nanoTime();
                new FixedTable<>(Pattern.class, 5, 5);
                long b = System.nanoTime();
                System.out.println("Manual creation via size took: " + (b - a) + " nanoseconds");
            }
        }
        System.out.println();
        System.out.println("-- ACCESS TESTS --");
        {
            FixedTable<Pattern> table = new FixedTable<>(new Pattern[5][5]);
            Pattern x = Pattern.compile(".+");
            Pattern y = Pattern.compile("^trees?");
            Pattern z = Pattern.compile("sa[r]+?dine[s]?$");
            table.set(0, 0, x);
            long a = System.nanoTime();
            table.set(0, 0, x);
            table.set(1, 0, y);
            table.set(2, 3, z);
            long b = System.nanoTime();
            table.get(0, 0);
            table.get(1, 0);
            table.get(2, 3);
            long c = System.nanoTime();
            table.getRow(1);
            long d = System.nanoTime();
            table.getColumn(0);
            long e = System.nanoTime();
            System.out.println("Setting took: " + (b - a) + " nanoseconds");
            System.out.println("Getting took: " + (c - b) + " nanoseconds");
            System.out.println("Retrieving row took: " + (d - c) + " nanoseconds");
            System.out.println("Retrieving column took: " + (e - d) + " nanoseconds");
        }
        System.out.println();
        System.out.println("-- SERIALISATION TESTS --");
        {
            String string;
            {
                FixedTable<Pattern> table = new FixedTable<>(new Pattern[5][5]);
                table.set(0, 0, Pattern.compile(".+"));
                table.set(1, 0, Pattern.compile("^trees?"));
                table.set(2, 3, Pattern.compile("sa[r]+?dine[s]?$"));
                table.serialise();
                long c = System.nanoTime();
                string = table.serialise();
                long d = System.nanoTime();
                System.out.println("Serialisation took: " + (d - c) + " nanoseconds");
            }
            {
                FixedTable.from(string);
                long a = System.nanoTime();
                FixedTable.from(string);
                long b = System.nanoTime();
                System.out.println("Deserialisation took: " + (b - a) + " nanoseconds");
            }
        }
        System.out.println();
        System.out.println("-- EXAMPLES --");
        {
            String string;
            {
                FixedTable<Pattern> table = new FixedTable<>(new Pattern[5][5]);
                table.set(0, 0, Pattern.compile(".+"));
                table.set(1, 0, Pattern.compile("^trees?"));
                table.set(2, 3, Pattern.compile("sa[r]+?dine[s]?$"));
                string = table.serialise();
            }
            {
                FixedTable<Pattern> table = FixedTable.from(string);
                System.out.println("(0, 0): " + table.get(0, 0));
                System.out.println("(1, 0): " + table.get(1, 0));
                System.out.println("(2, 3): " + table.get(2, 3));
            }
        }
        System.out.println();
        System.out.println("-- N-DIMENSIONAL TABLES --");
        {
            String string;
            {
                FixedNDimensionalTable<String> table = new FixedNDimensionalTable<>(String.class, 3, 3, 3, 3);
                table.set("hello", 1, 0, 0, 0);
                table.set("there", 0, 1, 0, 0);
                table.set("general", 0, 0, 1, 0);
                table.set("kenobi", 0, 0, 0, 1);
                System.out.println(Arrays.toString(table.asLinearRepresentation()));
                string = table.serialise();
            }
            {
                FixedNDimensionalTable<String> table = FixedNDimensionalTable.from(string);
                System.out.println("(1, 0, 0, 0): " + table.get(1, 0, 0, 0));
                System.out.println("(0, 1, 0, 0): " + table.get(0, 1, 0, 0));
                System.out.println("(0, 0, 1, 0): " + table.get(0, 0, 1, 0));
                System.out.println("(0, 0, 0, 1): " + table.get(0, 0, 0, 1));
                System.out.println("(0, 0, 0, 0): " + table.get(0, 0, 0, 0));
                System.out.println(Arrays.toString(table.asLinearRepresentation()));
            }
        }

        int requiredExperience = ExampleLevellingTable.TABLE.get(0, COMBAT, MELEE);
        int cowRequirements = ExampleLevellingTable.TABLE.get(99, FARMING, COWS);
    }

}
