# Mask

A project framework including simplifiers for boilerplate tasks as well as extensive libraries for various Java applications.

This framework is designed to massively simplify tasks in the project-creation process, as well as providing useful tools for tasks that new java users would find potentially difficult.

### Wiki

Mask has a full and detailed wiki with instructions and examples.

It can be found [here](https://gitlab.com/Pandaemonium/Mask/-/wikis/home).

Note: Mask has been moved to the Pandaemonium repository.
If you were using the old one, you will need to switch over!

### Features

* [x]  Simplify common boilerplate tasks
* [x]  Built-in configurable web server template system
* [x]  Configurable jSon-based web API system
* [x]  Special callback system for advanced tasks
* [x]  Easy reflection interface to avoid try/catching
* [x]  Additional class/object runtime debug system
* [x]  Position/tri-positional grid maps for 3D space work
* [x]  [Commander](Commander)
   * [x]  A version-independent command + argument handling system
   * [x]  Easily build commands in a simple format using consumers to schedule result code
   * [x]  Works with static (preset) arguments, and dynamic (accepting input) arguments
   * [x]  Works with **any** system, as long as there is a command sender object and a `String[]` of the arguments
   * [x]  Automatic argument input serialisation
   * [x]  Argument overloading
   * [x]  Automatic pattern generation, e.g. `/command <string> arg [<*int> <string...>]`
   * [x]  Argument/command aliases and descriptions
   * [x]  Fast, pre-parsed, type-safe argument handling
   * [x]  Fallback argument handling
* [x]  Completables and breakables, schedule tasks to be run in the event of success/failure
* [x]  Platform independency - BETA (Currently supports bukkit derivatives and bungeecord)
* [x]  Simple Java evaluator (using nashorn)
* [x]  Register commands without `plugin.yml` entries *(Bukkit)*
* [x]  [Item factory](Item-Factory) for faster creation/editing of items *(Bukkit)*
* [x]  [GUI menu creation system](GUI-Builder) *(Bukkit)*
* [x]  Automatic yaml config <-> class serialisation using annotations *(Bukkit)*
* [x]  Automatic object property expression generation - BETA *(Skript)*
* [x]  Automatic Skript addon registration *(Skript)*
* [x]  Exposition of some extra features from Skript *(Skript)*
* [ ]  Blind map converters
* [ ]  Socket network API
* [ ]  Full Skript syntax API *(Skript)*
* [ ]  Annotations for automatic Skript effect generation *(Skript)*
* [ ]  Improved debug systems
* [ ]  Simple storage API
* [ ]  Converters to and from Skript types *(Skript)*
* [ ]  Library for simpler regex queries
* [ ]  Version-supporting auto updater system


### Examples
Please refer to the [examples](src/examples/java/com/moderocky/example) package for some working test-cases of how to use basic features.

You may also find javadocs [here](https://moderocky.gitlab.io/Mask/), though referring to the code itself may be more useful.
 
### Using Maven

#### Linking to the repository
```xml
<repository>
    <id>pan-repo</id>
    <url>https://gitlab.com/api/v4/projects/18568066/packages/maven</url>
</repository>
```

#### Dependency information:
```xml
<dependency>
   <groupId>com.moderocky</groupId>
   <artifactId>Mask-#LIBRARY#</artifactId>
   <!-- Replace #LIBRARY# with the desired segment. -->
   <version>3.0.0-SNAPSHOT</version>
   <scope>compile</scope>
</dependency>
```

#### Shading and package relocation
This allows you to package the framework into your plugin.
The package relocation prevents conflicts.
```xml
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-shade-plugin</artifactId>
   <version>3.1.1</version>
   <executions>
      <execution>
         <phase>package</phase>
         <goals>
            <goal>shade</goal>
         </goals>
         <configuration>
            <relocations>
               <relocation>
                  <pattern>com.moderocky.mask</pattern>
                  <shadedPattern>your.package.here.mask</shadedPattern>
               </relocation>
            </relocations>
            <createDependencyReducedPom>false</createDependencyReducedPom>
         </configuration>
      </execution>
   </executions>
</plugin>
```

### Using Gradle

```
plugins {
    id 'com.github.johnrengelman.shadow' version '6.1.0'
    id 'java'
}

repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/18568066/packages/maven"
        name "pan-repo"
    }
}

dependencies {
    compile "com.moderocky:Mask-#LIBRARY#:3.0.0-SNAPSHOT"
}

shadowJar {
    relocate 'com.moderocky.mask', 'your.package.here.mask'
    relocate 'dev.moderocky.mask', 'your.package.here.mask'
}
```