package com.moderocky.mask.api.web;

import com.moderocky.mask.api.web.event.WebActionEvent;

public interface WebRequestListener<Event extends WebActionEvent> {

    void onEvent(Event event);

}
