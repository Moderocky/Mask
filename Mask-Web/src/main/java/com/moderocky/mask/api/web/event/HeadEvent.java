package com.moderocky.mask.api.web.event;

import com.moderocky.mask.api.web.Method;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

public class HeadEvent extends ResponseEvent {

    public HeadEvent(@NotNull WebServer<?> server, @NotNull WebConnection connection, @NotNull Socket socket, BufferedOutputStream outputStream, BufferedReader reader, PrintWriter writer, String requested) {
        super(server, connection, socket, Method.HEAD, outputStream, reader, writer, requested);
    }

}
