package com.moderocky.mask.api.web;

public enum Method {

    GET,
    POST,
    HEAD,
    JSQ

}
