package com.moderocky.mask.api.web.event;

import com.moderocky.mask.api.web.Method;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

public abstract class ResponseEvent extends WebActionEvent {

    private final BufferedOutputStream output;
    private final PrintWriter writer;
    private final BufferedReader reader;
    private final String requested;
    private final WebConnection connection;

    protected ResponseEvent(@NotNull WebServer<?> server, @NotNull WebConnection connection, @NotNull Socket socket, @NotNull Method method, BufferedOutputStream outputStream, BufferedReader reader, PrintWriter writer, String requested) {
        super(server, socket, method);
        this.output = outputStream;
        this.writer = writer;
        this.requested = requested;
        this.reader = reader;
        this.connection = connection;
    }

    public void sendResponse(String fileContent, String contentType) throws IOException {
        int fileLength = fileContent.length();
        byte[] fileData = fileContent.getBytes();
        writer.println("HTTP/1.1 200 OK");
        connection.outPrinter(writer, output, fileLength, contentType, fileData);
        setCancelled(true);
    }

    public void sendFileResponse(File file) throws IOException {
        int fileLength = (int) file.length();
        String content = connection.getContentType(file.getName());
        byte[] fileData = connection.readFileData(file, fileLength);
        writer.println("HTTP/1.1 200 OK");
        connection.outPrinter(writer, output, fileLength, content, fileData);
        setCancelled(true);
    }

    public BufferedReader getReader() {
        return reader;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public BufferedOutputStream getOutputStream() {
        return output;
    }

    public String getFileRequested() {
        return requested;
    }

}
