package com.moderocky.mask.api.web;

import com.moderocky.mask.annotation.DoNotInstantiate;
import com.moderocky.mask.annotation.Internal;
import com.moderocky.mask.annotation.Unsafe;
import com.moderocky.mask.api.web.event.GetEvent;
import com.moderocky.mask.api.web.event.HeadEvent;
import com.moderocky.mask.api.web.event.JSQEvent;
import com.moderocky.mask.api.web.event.PostEvent;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

@Internal
public abstract class WebConnection extends Thread implements Runnable {

    private final File webRoot;
    private final String defaultFile;
    private final String fileNotFound;
    private final String methodNotSupported;
    private final boolean debug;
    private final Socket connector;
    private final WebServer<? extends WebConnection> server;

    @Internal
    @DoNotInstantiate
    public WebConnection(Socket socket, WebServer<? extends WebConnection> webServer) {
        connector = socket;
        server = webServer;
        webRoot = server.getWebRootFolder();
        defaultFile = server.getDefaultFileName();
        fileNotFound = server.get404();
        methodNotSupported = server.getMethodNotSupportedFileName();
        debug = server.isDebug();
    }

    @SuppressWarnings("unused")
    public boolean isDebug() {
        return debug;
    }

    @SuppressWarnings("unused")
    public Socket getConnector() {
        return connector;
    }

    @SuppressWarnings("unused")
    public WebServer<? extends WebConnection> getServer() {
        return server;
    }

    @Override
    @Internal
    @Unsafe
    public void run() {
        server.addConnection(this);
        BufferedReader reader = null;
        PrintWriter writer = null;
        BufferedOutputStream outputStream = null;
        String fileRequested = null;

        try {
            InputStream stream = connector.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            writer = new PrintWriter(connector.getOutputStream());
            outputStream = new BufferedOutputStream(connector.getOutputStream());
            reader.mark(stream.available());

            List<String> lines = new ArrayList<>();
            for (int i = 0; i < 100; i++) { // Time-out
                if (!reader.ready()) break;
                String ln = reader.readLine();
                if (ln == null) break;
                lines.add(ln);
                if (!reader.ready()) break;
            }

            if (lines.size() > 0) {

                String input = lines.get(0);
                StringTokenizer parse = new StringTokenizer(input);
                String method = parse.nextToken().toUpperCase();
                fileRequested = parse.nextToken().toLowerCase();

                if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST") && !method.equals("JSQ")) {
                    if (debug) {
                        System.out.println("501 Not Implemented: " + method + " method.");
                    }

                    File file = new File(webRoot, methodNotSupported);
                    int fileLength = (int) file.length();
                    String contentMimeType = "text/html";
                    byte[] fileData = readFileData(file, fileLength);

                    writer.println("HTTP/1.1 501 Not Implemented");
                    outPrinter(writer, outputStream, fileLength, contentMimeType, fileData);

                } else {
                    if (fileRequested.endsWith("/") || fileRequested.endsWith("/?")) {
                        fileRequested += defaultFile;
                    }

                    File file = new File(webRoot, fileRequested);
                    int fileLength = (int) file.length();
                    String content = getContentType(fileRequested);

                    if (method.equals("GET")) {
                        GetEvent event = server.callEvent(new GetEvent(server, this, connector, outputStream, reader, writer, fileRequested));
                        if (!event.isCancelled()) {
                            byte[] fileData = readFileData(file, fileLength);

                            writer.println("HTTP/1.1 200 OK");
                            outPrinter(writer, outputStream, fileLength, content, fileData);
                        }
                    }
                    if (method.equals("POST")) {
                        PostEvent event = server.callEvent(new PostEvent(server, this, connector, outputStream, reader, writer, fileRequested));
                    }
                    if (method.equals("HEAD")) {
                        server.callEvent(new HeadEvent(server, this, connector, outputStream, reader, writer, fileRequested));
                    }
                    if (method.equals("JSQ")) {
                        server.callEvent(new JSQEvent(server, this, connector, outputStream, reader, writer, fileRequested));
                    }

                    if (debug) {
                        System.out.println("File " + fileRequested + " of type " + content + " returned.");
                    }

                }
            }

        } catch (FileNotFoundException fnfe) {
            try {
                if (writer != null && outputStream != null)
                    fileNotFound(writer, outputStream, fileRequested);
            } catch (IOException ignore) {
            }

        } catch (IOException e) {
            if (debug) {
                System.out.println("Connection file error.");
            }
        } finally {
            try {
                if (reader != null)
                    reader.close();
                if (writer != null)
                    writer.close();
                if (outputStream != null)
                    outputStream.close();
                connector.close(); // we close socket connection
            } catch (Exception ignore) {
            }
            if (debug) {
                System.out.println("Connection closed.");
            }

            server.removeConnection(this);
        }
    }

    @Internal
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public byte[] readFileData(File file, int fileLength) throws IOException {
        FileInputStream fileIn = null;
        byte[] fileData = new byte[fileLength];

        try {
            fileIn = new FileInputStream(file);
            fileIn.read(fileData);
        } finally {
            if (fileIn != null)
                fileIn.close();
        }

        return fileData;
    }

    @Internal
    public String getContentType(String fileRequested) {
        if (fileRequested.endsWith(".htm") || fileRequested.endsWith(".html") || fileRequested.endsWith(".xml") || fileRequested.endsWith(".xhtml") || fileRequested.endsWith(".php"))
            return "text/html";
        else
            return "text/plain";
    }

    @Internal
    public void fileNotFound(PrintWriter out, OutputStream dataOut, String fileRequested) throws IOException {
        File file = new File(webRoot, fileNotFound);
        int fileLength = (int) file.length();
        String content = "text/html";
        byte[] fileData = readFileData(file, fileLength);

        out.println("HTTP/1.1 404 File Not Found");
        outPrinter(out, dataOut, fileLength, content, fileData);

        if (debug) {
            System.out.println("File " + fileRequested + " not found.");
        }
    }

    @Internal
    public void outPrinter(PrintWriter out, OutputStream dataOut, int fileLength, String content, byte[] fileData) throws IOException {
        out.println("Date: " + new Date());
        out.println("Content-type: " + content);
        out.println("Content-length: " + fileLength);
        out.println();
        out.flush();

        dataOut.write(fileData, 0, fileLength);
        dataOut.flush();
    }

}