package com.moderocky.mask.annotation;

import java.lang.annotation.*;

/**
 * A class or constructor that should never be used by external classes.
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.CONSTRUCTOR})
public @interface DoNotInstantiate {

    /**
     * @return The name of the getter to be used to require an instance of this.
     */
    String accessMethod() default "None";

}
