package com.moderocky.mask.annotation;


import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * Marks a method as being run asynchronously.
 * This means that the return may not be readily available or viable.
 * Often these methods will have a result consumer or completable future.
 */
@Target({METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@SuppressWarnings("unused")
public @interface Asynchronous {

    boolean threadSafe() default true;

}
