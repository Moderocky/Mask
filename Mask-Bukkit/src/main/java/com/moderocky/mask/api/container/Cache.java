package com.moderocky.mask.api.container;

import java.util.*;

/**
 * A cache for minimising retrievals.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public abstract class Cache<K, V> extends HashMap<K, V> {

    /**
     * This will return the cached value, or query the value if it is not already present.
     *
     * @param key The key.
     * @return The objectp
     */
    @Override
    public V get(Object key) {
        if (!containsKey(key)) {
            try {
                K k = (K) key;
                put(k, getIfAbsent(k));
            } catch (Throwable ignore) {
            }
        }
        return super.get(key);
    }

    /**
     * The method to get the value given the key.
     *
     * @param key Key.
     * @return value
     */
    public abstract V getIfAbsent(K key);

    public void update() {
        List<K> keySet = new ArrayList<>(keySet());
        keySet.forEach(k -> replace(k, getIfAbsent(k)));
    }

    public void invalidate() {
        clear();
    }

    static class Node<K, V> implements Entry<K, V> {
        final int hash;
        final K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final K getKey() {
            return key;
        }

        public final V getValue() {
            return value;
        }

        public final String toString() {
            return key + "=" + value;
        }

        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Map.Entry) {
                Entry<?, ?> e = (Entry<?, ?>) o;
                return Objects.equals(key, e.getKey()) &&
                        Objects.equals(value, e.getValue());
            }
            return false;
        }
    }

}
