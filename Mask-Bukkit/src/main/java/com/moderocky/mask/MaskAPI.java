package com.moderocky.mask;

import ch.njol.skript.ScriptLoader;
import ch.njol.skript.SkriptCommand;
import ch.njol.skript.lang.Condition;
import com.google.common.base.CaseFormat;
import com.moderocky.mask.annotation.API;
import com.moderocky.mask.internal.event.DummyEvent;
import com.moderocky.mask.internal.event.DummyPlayerEvent;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerEvent;

import javax.annotation.Nullable;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
@API
public class MaskAPI {

    protected MaskAPI() {
    }

    @API
    @com.moderocky.mask.annotation.Unsafe
    public Unsafe getUnsafe() {
        return new Unsafe();
    }

    @API
    public File getScript(String name) {
        if (Bukkit.getPluginManager().getPlugin("Skript") == null) return null;
        return SkriptCommand.getScriptFromName(name);
    }

    @API
    public String[] getSyntaxes(String entry, String... defaults) {
        FileConfiguration file = Mask.getMask().getAddonConfig();
        assert file != null;
        List<String> strings = file.getStringList(entry);
        if (strings.size() < 1)
            return defaults;
        return strings.toArray(new String[0]);
    }

    @API
    public String[] getCommands(String scriptName) {
        if (!Mask.getMask().hasSkript()) return null;
        if (Bukkit.getPluginManager().getPlugin("Skript") == null) return null;
        List<String> commands = new ArrayList<>();
        try {
            Map<String, Set<String>> commandMap = new Mirror<>(ScriptLoader.class).<Map<String, Set<String>>>field("commandNames").get();
            Set<String> strings = commandMap.get(scriptName);
            if (strings != null)
                commands.addAll(strings);
        } catch (Exception e) {
            //
        }
        return commands.toArray(new String[0]);
    }

    public static class Unsafe {

        public Unsafe() {

        }

        public boolean check(String cond) {
            Condition condition = Condition.parse(cond, "An error has occurred.");
            return condition.run(new DummyEvent());
        }

        public boolean check(String cond, @Nullable Event event, @Nullable String debug) {
            if (debug == null) debug = "An error has occurred.";
            if (event == null) event = new DummyEvent();
            Condition condition = Condition.parse(cond, debug);
            return condition.run(event);
        }

        public boolean check(String cond, Player pov, @Nullable PlayerEvent event, @Nullable String debug) {
            if (debug == null) debug = "An error has occurred.";
            if (event == null) event = new DummyPlayerEvent(pov);
            Condition condition = Condition.parse(cond, debug);
            return condition.run(event);
        }

        public String[] getPropertyGetters(Class<?> clarse) {
            List<String> strings = new ArrayList<>();
            for (Method method : clarse.getMethods()) {
                if (method.getName().matches("get[A-Z].*")) {
                    String string = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, method.getName()).replace("_", " ").replaceFirst("get ", "");
                    strings.add(string);
                }
            }
            return strings.toArray(new String[0]);
        }

        public String[] getPropertySetters(Class<?> clarse) {
            List<String> strings = new ArrayList<>();
            for (Method method : clarse.getMethods()) {
                if (method.getName().matches("set[A-Z].*")) {
                    String string = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, method.getName()).replace("_", " ").replaceFirst("get ", "");
                    strings.add(string);
                }
            }
            return strings.toArray(new String[0]);
        }
    }

}
