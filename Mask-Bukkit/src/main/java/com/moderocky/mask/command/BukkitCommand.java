package com.moderocky.mask.command;

import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.command.annotation.Command;
import com.moderocky.mask.command.annotation.*;
import com.moderocky.mask.internal.utility.MaskMetrics;
import dev.moderocky.mirror.MethodMirror;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;

public abstract class BukkitCommand implements CommandExecutor, TabCompleter {

    private final Commander.CommandSingleAction<CommandSender> defaultAction;
    private final MagicMap<Commander.ArgumentEntry, Commander.CommandAction<CommandSender>> argumentMap = new MagicMap<>();
    private final Commander<CommandSender> commander;
    private String[] command;
    private String description;
    private String permission;
    private String permissionMessage;
    private String usage;

    static {
        MaskMetrics.attemptStartup();
    }

    {
        Mirror<? extends BukkitCommand> mirror = new Mirror<>(this);
        if (!mirror.hasAnnotation(Command.class))
            throw new IllegalCommandException("No command annotations were provided!");
        {
            MethodMirror<?>[] mirrors = mirror.getMethodMirrors(Default.class);
            if (mirrors.length < 1) throw new IllegalCommandException("No default behaviour was provided!");
            MethodMirror<?> method = mirrors[0];
            if (method.getParameterCount() != 1)
                throw new IllegalCommandException("Default method should accept only a command sender!");
            defaultAction = method::invoke;
        }
        {
            MethodMirror<?>[] mirrors = mirror.getMethodMirrors(Arguments.class);
            for (MethodMirror<?> methodMirror : mirrors) {
                Arguments arguments = methodMirror.getAnnotation(Arguments.class);
                new Commander.ArgumentEntry();
                MagicList<Argument<?>> args = new MagicList<>();
                for (String string : arguments.literal()) {
                    args.add(new ArgLiteral(string));
                }
                for (Class<? extends Argument<?>> cls : arguments.value()) {
                    try {
                        args.add(cls.newInstance());
                    } catch (Throwable ignore) {
                    }
                }
                Commander.ArgumentEntry entry = new Commander.ArgumentEntry(args.toArray(new Argument[0]));
                Commander.CommandBiAction<CommandSender> action;
                if (methodMirror.hasAnnotation(Permission.class)) {
                    final String permission = methodMirror.getAnnotation(Permission.class).value();
                    action = (sender, input) -> {
                        if (sender.hasPermission(permission)) {
                            MagicList<Object> objects = new MagicList<>();
                            objects.add(sender);
                            objects.addAll(input);
                            methodMirror.invoke(objects.toArray(new Object[0]));
                        } else if (permissionMessage != null && !permissionMessage.isEmpty()) {
                            sender.sendMessage(permissionMessage);
                        } else {
                            PluginCommand c = Bukkit.getServer().getPluginCommand(command[0]);
                            if (c != null && c.getPermissionMessage() != null)
                                sender.sendMessage(c.getPermissionMessage());
                        }
                    };
                } else {
                    action = (sender, input) -> {
                        MagicList<Object> objects = new MagicList<>();
                        objects.add(sender);
                        objects.addAll(input);
                        methodMirror.invoke(objects.toArray(new Object[0]));
                    };
                }
                argumentMap.put(entry, action);
            }
        }
        commander = new Commander<CommandSender>() {
            {
                tree.putAll(argumentMap);
                compile();
            }

            @Override
            protected CommandImpl create() {
                Command command = mirror.getAnnotation(Command.class);
                MagicStringList list = new MagicStringList(command.value());
                return command(list.remove(0), list.toArray());
            }

            @Override
            public CommandSingleAction<CommandSender> getDefault() {
                return defaultAction;
            }
        };
        mirror.ifHasAnnotation(Command.class, note -> command = note.value());
        mirror.ifHasAnnotation(Description.class, note -> description = note.value());
        mirror.ifHasAnnotation(Usage.class, note -> usage = note.value());
        mirror.ifHasAnnotation(Permission.class, note -> {
            permission = note.value();
            permissionMessage = note.message();
        });
    }

    public BukkitCommand() {

    }

    public BukkitCommand(Plugin plugin) {
        register(plugin);
    }

    public final synchronized void register(Plugin plugin) {
        final CommandMap commandMap = new Mirror<>(Bukkit.getServer()).<CommandMap>field("commandMap").get();
        try {
            Constructor<PluginCommand> commandConstructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            if (!commandConstructor.isAccessible())
                commandConstructor.setAccessible(true);
            PluginCommand pluginCommand = commandConstructor.newInstance(commander.getCommand(), plugin);
            pluginCommand.setAliases(commander.getAliases());
            pluginCommand.setDescription(description);
            pluginCommand.setPermission(permission);
            pluginCommand.setPermissionMessage(permissionMessage == null ? null : permissionMessage.isEmpty() ? null : permissionMessage);
            pluginCommand.setUsage(usage);
            pluginCommand.register(commandMap);
            if (commandMap.register(commander.getCommand(), plugin.getName(), pluginCommand)) {
                pluginCommand.setExecutor(this);
                pluginCommand.setTabCompleter(this);
            } else {
                org.bukkit.command.Command com = commandMap.getCommand(pluginCommand.getName());
                if (com instanceof PluginCommand) {
                    ((PluginCommand) com).setExecutor(this);
                    ((PluginCommand) com).setTabCompleter(this);
                }
                Bukkit.getLogger().log(Level.WARNING, "A command '/" + commander.getCommand() + "' is already defined!");
                Bukkit.getLogger().log(Level.WARNING, "As this cannot be replaced, the executor will be overridden.");
                Bukkit.getLogger().log(Level.WARNING, "To avoid this warning, please do not add WrappedCommands to your plugin.yml.");
            }
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull org.bukkit.command.Command command, @NotNull String s, @NotNull String[] strings) {
        return commander.execute(commandSender, strings);
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull org.bukkit.command.Command command, @NotNull String s, @NotNull String[] strings) {
        return commander.getTabCompletions(strings);
    }
}
