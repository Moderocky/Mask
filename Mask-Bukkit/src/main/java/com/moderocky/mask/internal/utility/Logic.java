package com.moderocky.mask.internal.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Logic {

    public static <X> List<X[]> getPairs(Collection<X> collection) {
        List<X> list = new ArrayList<>(collection);
        List<X[]> xes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                xes.add((X[]) new Object[]{list.get(i), list.get(j)});
            }
        }
        return xes;
    }

    public static List<String> getStripped(Collection<String> collection) {
        List<String> strings = new ArrayList<>(collection);
        for (String string : new ArrayList<>(strings)) {
            strings.removeIf(string::contains);
        }
        return strings;
    }

    public static <T> List<T> withoutLast(T[] parts) {
        int length = parts.length;
        List<T> tList;
        if (parts.length < 2) {
            tList = new ArrayList<>();
        } else {
            tList = new ArrayList<>(Arrays.asList(parts));
            tList.remove(tList.size() - 1);
        }
        return tList;
    }

}
