package com.moderocky.mask.internal.event;

import com.moderocky.mask.annotation.Internal;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

@Internal
public class DummyEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public DummyEvent() {

    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

}
