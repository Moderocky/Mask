package com.moderocky.mask.internal.event;

import com.moderocky.mask.annotation.Internal;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.jetbrains.annotations.NotNull;

@Internal
public class DummyPlayerEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    private Player player;

    public DummyPlayerEvent(@NotNull org.bukkit.entity.Player who) {
        super(who);
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

}
