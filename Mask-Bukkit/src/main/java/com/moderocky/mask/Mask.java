package com.moderocky.mask;

import ch.njol.skript.Skript;
import com.moderocky.mask.annotation.API;
import com.moderocky.mask.annotation.Internal;
import com.moderocky.mask.annotation.Unsafe;
import com.moderocky.mask.internal.utility.MaskMetrics;
import com.moderocky.mask.template.BukkitPlugin;
import com.moderocky.mask.template.IPlugin;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginLoader;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

@API
@SuppressWarnings("unused")
public class Mask implements Plugin {

    private static Mask mask = new Mask();
    private static IPlugin plugin;
    private static YamlConfiguration addonConfig;

    private MaskMetrics metrics;

    /**
     * @return the current Mask instance.
     */
    @API
    @NotNull
    public static Mask getMask() {
        return mask;
    }

    /**
     * Used if you aren't using the mask abstract template. Remember, this will break some things. :)
     *
     * @param plugin a plugin.
     */
    @API
    @Unsafe
    @Deprecated
    public static void register(JavaPlugin plugin) {
        mask = new Mask();
        MaskMetrics.attemptStartup(plugin);
    }

    @Internal
    @Unsafe
    public static void setMask(Mask mask, IPlugin plugin) {
        Mask.mask = mask;
        Mask.plugin = plugin;
        if (plugin instanceof JavaPlugin) MaskMetrics.attemptStartup((JavaPlugin) plugin);
    }

    /**
     * @return the IPlugin - a nondescript platform independent plugin
     */
    @API
    public static IPlugin getPlugin() {
        return plugin;
    }

    /**
     * @return The API, used for cool stuff.
     */
    @API
    @NotNull
    public MaskAPI getAPI() {
        return new MaskAPI();
    }

    @API
    @Nullable
    public FileConfiguration getAddonConfig() {
        return addonConfig;
    }

    @Unsafe
    public void completeRegistration() {
        if (hasSkript() && plugin.isBukkit() && plugin instanceof BukkitPlugin) {
            try {
                ((BukkitPlugin) getPlugin()).getAddon().loadClasses("com.moderocky.mask", "registry");
            } catch (Throwable ignore) {
            }
        }
    }

    /**
     * @return true if Skript is present and also accessible for syntax registration.
     */
    @API
    public boolean hasSkript() {
        return (Bukkit.getPluginManager().getPlugin("Skript") != null && Mirror.classExists("ch.njol.skript.Skript") && Skript.isAcceptRegistrations());
    }

    @Override
    public @NotNull File getDataFolder() {
        return new File("plugins/Mask/");
    }

    @Override
    public @NotNull PluginDescriptionFile getDescription() {
        throw new RuntimeException("This should not be used!");
    }

    @Override
    public @NotNull FileConfiguration getConfig() {
        throw new RuntimeException("This should not be used!");
    }

    @Override
    public @Nullable InputStream getResource(@NotNull String filename) {
        return getClass().getClassLoader().getResourceAsStream(filename);
    }

    @Override
    public void saveConfig() {

    }

    @Override
    public void saveDefaultConfig() {

    }

    @Override
    public void saveResource(@NotNull String resourcePath, boolean replace) {

    }

    @Override
    public void reloadConfig() {

    }

    @Override
    public @NotNull PluginLoader getPluginLoader() {
        throw new RuntimeException("This should not be used!");
    }

    @Override
    public @NotNull Server getServer() {
        return Bukkit.getServer();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onLoad() {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public boolean isNaggable() {
        return false;
    }

    @Override
    public void setNaggable(boolean canNag) {

    }

    @Override
    public @Nullable ChunkGenerator getDefaultWorldGenerator(@NotNull String worldName, @Nullable String id) {
        return null;
    }

    @Override
    public @NotNull Logger getLogger() {
        return Bukkit.getLogger();
    }

    @Override
    public @NotNull String getName() {
        return "Mask";
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        throw new RuntimeException("This should not be used!");
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        throw new RuntimeException("This should not be used!");
    }
}
