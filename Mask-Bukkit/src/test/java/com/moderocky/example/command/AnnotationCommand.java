package com.moderocky.example.command;

import com.moderocky.mask.command.ArgInteger;
import com.moderocky.mask.command.ArgString;
import com.moderocky.mask.command.ArgStringFinal;
import com.moderocky.mask.command.BukkitCommand;
import com.moderocky.mask.command.annotation.*;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

@Command({"mycommand", "alias1", "alias_2"})
@Description("A cool command!")
@Permission("myplugin.mycommand")
@Usage("/mycommand help")
public class AnnotationCommand extends BukkitCommand {

    @Default
    public void theDefaultThing(CommandSender sender) {
        sender.sendMessage("hi, you did a command");
    }

    @Arguments(literal = {"help"})
    public void help(CommandSender sender) {
        sender.sendMessage("help message");
    }

    @Arguments({ArgInteger.class, ArgString.class})
    public void thingy(CommandSender sender, Integer integer, String string) {
    }

    @Arguments(literal = "broadcast", value = {ArgStringFinal.class})
    public void broadcast(CommandSender sender, String string) {
        Bukkit.broadcastMessage(string);
    }

}
