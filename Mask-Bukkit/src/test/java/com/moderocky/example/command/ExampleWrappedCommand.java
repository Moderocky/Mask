package com.moderocky.example.command;

import com.moderocky.mask.template.WrappedCommand;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ExampleWrappedCommand implements WrappedCommand {

    @Override
    public @NotNull List<String> getAliases() {
        return new ArrayList<>();
    }

    @Override
    public @NotNull String getUsage() {
        return "/settlements";
    }

    @Override
    public @NotNull String getDescription() {
        return "The main command for the Settlements plugin.";
    }

    @Override
    public @Nullable String getPermission() {
        return "settlements.command.settlements";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public @NotNull String getCommand() {
        return "settlements";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) {
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        sender.spigot().sendMessage(new ComponentBuilder("")
                .append("Settlements! :)")
                .create()
        );
        return true;
    }

}
