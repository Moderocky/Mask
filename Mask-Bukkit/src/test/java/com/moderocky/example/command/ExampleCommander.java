package com.moderocky.example.command;

import com.moderocky.mask.command.*;
import com.moderocky.mask.template.WrappedCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExampleCommander extends Commander<Player> implements WrappedCommand {

    @Override
    @SuppressWarnings("all")
    public @NotNull Commander.CommandImpl create() {
        return command("test") // The main command label
                // STATIC ARGS EXAMPLES
                .arg(sender -> sender.sendMessage("help message"), "blob")
                .arg("help", sender -> sender.sendMessage("help message")) // ./test help -> sends message
                .arg("test", // ./test test -> error
                        arg("hi1", sender -> sender.sendMessage("hello 1")), // ./test test hi1 -> sends message
                        arg("hi2", sender -> sender.sendMessage("hello 2")), // ./test test hi2 -> sends message
                        arg("hi3", sender -> sender.sendMessage("hello 3"))  // ./test test hi3 -> sends message
                )
                .arg("a", // ./test a -> error
                        arg("b", // ./test a b -> error
                                arg("c", // ./test a b c -> error
                                        arg("1", Player::resetTitle), // ./test a b c 1 -> action
                                        arg("2", Player::resetPlayerTime), // ./test a b c 2 -> action
                                        arg("3", Player::resetPlayerWeather)  // ./test a b c 3 -> action
                                )
                        ),
                        arg("2", // ./test a 2 -> error
                                arg("3", sender -> sender.sendMessage("hi")), // ./test a 2 3 -> sends message
                                arg("4", sender -> sender.sendMessage("hi 2?")) // ./test a 2 3 -> sends message
                        ),
                        arg("2", // ./test a 2 -> error, also unreachable
                                arg("3", Player::resetTitle) // ./test a 2 3 -> unreachable (2 3 is already a thing)
                        )
                )
                // DYNAMIC ARGS EXAMPLES
                // Two patterns, either (arg, consumer) or (consumer, args...)
                .arg( // ./test <player> -> sends player arg's name to sender
                        new ArgPlayer(),
                        (sender, input) -> sender.sendMessage(((Player) input[0]).getDisplayName())
                )
                .arg( // ./test <number> -> sends number name to sender
                        new ArgNumber(),
                        (sender, input) -> sender.sendMessage(input[0].toString())
                )
                .arg( // ./test <string> [string]
                        (sender, input) -> sender.sendMessage(input[0].toString() + " and " + input[1] + " might be null."),
                        new ArgString(),
                        new ArgString().setRequired(false) // the corresponding array value will be null if unset.
                )
                .arg( // ./test <string> [string]
                        (sender, input) -> sender.sendMessage(input[0].toString()),
                        new ArgPlayer(),
                        new ArgString().setRequired(false)
                )
                .arg("hi", // ./test hi
                        arg( // ./test hi <number>
                                (sender, input) -> sender.sendMessage("today's number is " + input[0]),
                                new ArgNumber()
                        ),
                        arg( // ./test hi <player> [string]
                                (sender, input) -> sender.sendMessage(input[0].toString()),
                                new ArgPlayer(),
                                new ArgString().setRequired(false)
                        )
                )
                .arg("t_1", // ./test t_1
                        arg("t_2", // ./test t_1 t_2
                                arg( // ./test t_1 t_2 <number>
                                        (sender, input) -> sender.sendMessage("today's number is " + input[0]),
                                        new ArgNumber()
                                ),
                                arg( // ./test t_1 t_2 <player> [string]
                                        (sender, input) -> sender.sendMessage(input[0].toString()),
                                        new ArgPlayer(),
                                        new ArgPlayer()
                                )
                        )
                )
                .arg("hi",
                        arg( // ./test hi <number>
                                (sender, input) -> sender.sendMessage("today's number is " + input[0]),
                                new ArgNumber()
                        )
                )
                .arg( // ./test <int> <int> <int>
                        (sender, arg) -> {
                            sender.teleport(new Location(sender.getWorld(), (int) arg[0], (int) arg[1], (int) arg[2]));
                        },
                        new ArgInteger(),
                        new ArgInteger(),
                        new ArgInteger()
                );
    }

    @Override
    public @NotNull CommandSingleAction<Player> getDefault() { // This provides a help message with clickable options for ALL possible arguments
        return player -> {
            ComponentBuilder builder = new ComponentBuilder("Help for /" + getCommand() + ": ")
                    .bold(true)
                    .color(ChatColor.WHITE);
            for (String pattern : getPatterns()) {
                builder
                        .append(System.lineSeparator())
                        .reset()
                        .append("/" + getCommand())
                        .color(ChatColor.AQUA)
                        .append(" ")
                        .append(pattern)
                        .event(new ClickEvent((pattern.contains("[") || pattern.contains("<")) ? ClickEvent.Action.SUGGEST_COMMAND : ClickEvent.Action.RUN_COMMAND, "/" + getCommand() + " " + pattern.replaceFirst("(<.*|\\[.*)", "")))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText((pattern.contains("[") || pattern.contains("<")) ? "Click to suggest." : "Click to run.", ChatColor.AQUA)))
                        .color(ChatColor.WHITE);
            }
            player.spigot().sendMessage(builder.create());
        };
    }

    @Override
    public @Nullable String getPermission() {
        return "test.command.permission";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("testing", "testcommand");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Description here";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) { // Not needed - we're using the parent.
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull org.bukkit.command.Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) {
            return execute((Player) sender, args);
        }
        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull org.bukkit.command.Command command, @NotNull String alias, @NotNull String[] args) {
        List<String> strings = getPossibleArguments(String.join(" ", args));
        if (strings == null || strings.isEmpty()) return null;
        final List<String> completions = new ArrayList<>();
        StringUtil.copyPartialMatches(args[args.length - 1], strings, completions);
        Collections.sort(completions);
        return completions;
    }

}
