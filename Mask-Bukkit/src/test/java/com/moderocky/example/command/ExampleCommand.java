package com.moderocky.example.command;

import com.moderocky.mask.template.WrappedCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExampleCommand implements WrappedCommand {

    @Override
    public @NotNull String getCommand() {
        return "commandlabel";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) {
        if (i == 1)
            return Arrays.asList("arg", "another_arg");
        return null;
    }

    @Override
    public @NotNull BaseComponent[] getHelpMessage(int i, @NotNull String[] args) {
        return new ComponentBuilder("I'm a help message!").color(ChatColor.LIGHT_PURPLE).create();
    }

    @Override
    public @NotNull List<String> getAliases() {
        return new ArrayList<>();
    }

    @Override
    public @NotNull String getUsage() {
        return "/commandlabel";
    }

    @Override
    public @NotNull String getDescription() {
        return "My description";
    }

    @Override
    public @Nullable String getPermission() {
        return null;
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        // Your normal command-y stuff goes here. :)
        return true;
    }

}
