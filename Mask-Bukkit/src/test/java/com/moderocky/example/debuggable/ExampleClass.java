package com.moderocky.example.debuggable;

import org.bukkit.block.Block;

import java.util.List;

// This ENTIRE class would be de-bugged.
public class ExampleClass {

    private final static String string = "strung";

    private final String blob = "blob";

    private final int anInt = 66;

    public ExampleClass(int i, List<Block> blockList) {

    }

    public static void testMethod(Boolean boo) {

    }

}
