package com.moderocky.mask.api.proc;

import com.moderocky.mask.api.proc.notation.Async;
import com.moderocky.mask.api.proc.notation.Sync;

@FunctionalInterface
public interface Task extends Runnable {

    default boolean isAsync() {
        return this.getClass().isAnnotationPresent(Async.class);
    }

    default boolean isSync() {
        return this.getClass().isAnnotationPresent(Sync.class);
    }

}
