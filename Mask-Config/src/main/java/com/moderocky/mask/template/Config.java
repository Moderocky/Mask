package com.moderocky.mask.template;

import com.google.common.base.CaseFormat;
import com.moderocky.mask.annotation.API;
import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.internal.utility.ConfigFileManager;
import dev.moderocky.mirror.FieldMirror;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@SuppressWarnings("all")
public interface Config {

    @SuppressWarnings("all")
    default void save() {
        File file = getFile();
        FileConfiguration config = getConfig();
        Mirror<Config> mirror = new Mirror<>(this);
        mirror.ifHasAnnotation(Configurable.Overwrite.class, overwrite -> {
            if (overwrite.value()) ConfigFileManager.write(file, "");
        });
        HashMap<String, String[]> comments = new HashMap<>();
        for (FieldMirror<?> field : mirror.getFieldMirrors()) {
            if (field.isTransient() || field.isStatic()) continue;
            Configurable configurable = field.getAnnotation(Configurable.class);
            String path = getFieldPath(field.getLiteral(), configurable != null ? configurable.value() : "");
            Object value;
            if (field.isPrimitive()) {
                value = field.get();
            } else if (field.isArray()) {
                List<String> list = new ArrayList<>();
                Object[] objects = ((FieldMirror<Object[]>) field).get();
                for (Object object : objects) list.add(object.toString());
                value = list;
            } else if (field.isCollection()) {
                value = field.get();
            } else if (field.isEnum()) {
                value = field.get().toString();
            } else if (field.isMap()) {
                value = null;
                for (Map.Entry<?, ?> entry : ((Map<?, ?>) field.get()).entrySet()) {
                    try {
                        config.set(path + "." + entry.getKey().toString(), entry.getValue());
                    } catch (Throwable throwable) {
                        Bukkit.getLogger().log(Level.WARNING, "Config saving error: " + entry.getValue().toString() + " could not be stored.");
                    }
                }
            } else {
                value = serialise(field.getLiteral(), field.get());
            }
            if (value != null) config.set(path, value);
            field.ifHasAnnotation(Configurable.Comment.class, comment -> {
                if (comment.value().length < 1) return;
                comments.put(path.toLowerCase(), comment.value());
            });
        }
        ConfigFileManager.save(config, file);
        addComments(comments);
    }

    @SuppressWarnings("all")
    default Config load() {
        FileConfiguration config = getConfig();
        Mirror<Config> mirror = new Mirror<>(this);
        for (FieldMirror<?> field : mirror.getFieldMirrors(Configurable.class)) {
            Configurable configurable = field.getAnnotation(Configurable.class);
            if (configurable.override()) continue;
            String path = getFieldPath(field.getLiteral(), configurable.value());
            if (field.isMap()) {
                HashMap map = new HashMap();
                ConfigurationSection section = config.getConfigurationSection(path);
                if (section != null && section.getKeys(false).size() > 0) {
                    for (String key : section.getKeys(false)) {
                        try {
                            map.put(key, section.get(key));
                        } catch (Throwable throwable) {
                            Bukkit.getLogger().log(Level.WARNING, "Config loading error: " + section.get(key).toString() + " could not be loaded.");
                        }
                    }
                    ((FieldMirror<Map>) field).set(this, map);
                }
            } else if (config.isSet(path)) {
                Object value;
                if (field.isEnum()) {
                    value = new Mirror<>(field.getType()).getEnum((config.getString(path)).toUpperCase().replace(" ", "_"));
                } else if (config.get(path) instanceof String) {
                    value = matchString(field.getLiteral(), config.get(path));
                } else {
                    value = adjustValue(field.getLiteral(), config.get(path));
                }
                value = deserialise(field.getLiteral(), value);
                ((FieldMirror<Object>) field).set(value);
            }
        }
        save();
        return this;
    }

    default Object serialise(Field field, Object value) {
        Configurable.Serialise serialise = field.getAnnotation(Configurable.Serialise.class);
        if (serialise == null || !serialise.stringify()) return value;
        return value.toString();
    }

    default Object deserialise(Field field, Object value) {
        Mirror<?> mirror = new Mirror<>(field);
        if (!mirror.hasAnnotation(Configurable.Serialise.class)) return value;
        Configurable.Serialise serialise = mirror.getAnnotation(Configurable.Serialise.class);
        return mirror.hasMethod(serialise.method(), value.getClass()) ? mirror.invoke(serialise.method(), value) : value.toString();
    }

    default void addComments(@NotNull HashMap<String, String[]> comments) {
        // This has been silently removed for now.
    }

    default Object matchString(Field field, Object value) {
        Configurable.Regex matcher = field.getAnnotation(Configurable.Regex.class);
        if (matcher == null) return value;
        String regex = matcher.matcher();
        String v = (String) value;
        if (v.matches(regex)) return value;
        return matcher.alternative();
    }

    default Object adjustValue(Field field, Object value) {
        Configurable.Bounded bounded = field.getAnnotation(Configurable.Bounded.class);
        if (bounded == null) return value;
        if (value instanceof Integer) {
            int v = (Integer) value;
            if (v > bounded.maxValue()) v = Math.min(v, (int) Math.floor(bounded.maxValue()));
            if (v < bounded.minValue()) v = Math.max(v, (int) Math.ceil(bounded.minValue()));
            return v;
        } else if (value instanceof Long) {
            long v = (Long) value;
            if (v > bounded.maxValue()) v = Math.min(v, (long) Math.floor(bounded.maxValue()));
            if (v < bounded.minValue()) v = Math.max(v, (long) Math.ceil(bounded.minValue()));
            return v;
        } else if (value instanceof Double) {
            double v = (Double) value;
            if (v > bounded.maxValue()) v = Math.min(v, bounded.maxValue());
            if (v < bounded.minValue()) v = Math.max(v, bounded.minValue());
            return v;
        } else if (value instanceof Float) {
            float v = (Float) value;
            if (v > bounded.maxValue()) v = Math.min(v, (float) bounded.maxValue());
            if (v < bounded.minValue()) v = Math.max(v, (float) bounded.minValue());
            return v;
        }
        return value;
    }

    default String convertFieldName(Field field) {
        String fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
        try {
            Configurable.Keyed keyed = field.getAnnotation(Configurable.Keyed.class);
            if (keyed != null && keyed.value().length() > 0 && keyed.value().matches("^[a-zA-Z0-9_-]+$")) {
                fieldName = keyed.value();
            }
        } catch (Exception ignore) {
        }
        return fieldName;
    }

    default String getFieldPath(Field field, String parent) {
        String path;
        String fieldName = convertFieldName(field);
        if (parent.length() > 0) {
            path = parent;
            if (!path.endsWith("."))
                path = path + ".";
            path = path + fieldName;

        } else {
            path = fieldName;
        }
        return path.replaceAll("\\.\\.", ".");
    }

    /**
     * @return The folder path, e.g. "plugins/YourPlugin/"
     */
    @API
    @NotNull String getFolderPath();

    /**
     * @return The file name, e.g. "config.yml"
     */
    @API
    @NotNull String getFileName();

    default FileConfiguration getConfig() {
        return ConfigFileManager.getFile(getFolderPath(), getFileName());
    }

    default File getFile() {
        return new File(getFolderPath(), getFileName());
    }

}
